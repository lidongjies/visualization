export default class Timing {
	constructor({ duration, iterations = 1, easing = p => p }) {
		this.duration = duration;
		this.iterations = iterations;
		this.easing = easing;
		this.startTime = Date.now();
	}

	get time() {
		return Date.now() - this.startTime;
	}

	get p() {
		const progress = Math.min(this.time / this.duration, this.iterations);
		return this.finished ? 1 : this.easing(progress % 1);
	}

	get finished() {
		return this.time / this.duration >= this.iterations;
	}
}
