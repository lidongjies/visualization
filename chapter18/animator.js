import Timing from './timing.js';

export default class Animator {
	constructor({ duration, iterations, easing }) {
		this.timing = { duration, iterations, easing };
	}

	animate(target, update) {
		const timing = new Timing(this.timing);
		let frameIndex = 0;
		return new Promise(resolve => {
			function next() {
				if (update({ target, timing, frameIndex }) !== false && !timing.finished) {
					requestAnimationFrame(next);
				} else {
					resolve(timing);
				}
				frameIndex++;
			}
			next();
		});
	}
}
