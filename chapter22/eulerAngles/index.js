import {
	Mesh,
	Program,
	Renderer,
	Transform,
	Texture,
	Camera,
	Geometry
} from '../../common/lib/ogl/index.mjs';
import * as dat from '../../common/lib/dat.gui.js';

async function loadShader(src) {
	const data = await (await fetch(src)).text();
	return data;
}

const canvas = document.getElementById('paper');
const renderer = new Renderer({ canvas, width: 512, height: 512 });

const gl = renderer.gl;
gl.clearColor(1, 1, 1, 1);

const camera = new Camera(gl, { fov: 35 });
camera.position.set(0, 0, 10);
camera.lookAt([0, 0, 0]);

const scene = new Transform();

async function loadModel(src) {
	const data = await (await fetch(src)).json();

	const geometry = new Geometry(gl, {
		position: { size: 3, data: new Float32Array(data.position) },
		uv: { size: 2, data: new Float32Array(data.uv) },
		normal: { size: 3, data: new Float32Array(data.normal) }
	});

	return geometry;
}

function loadTexture(src) {
	const texture = new Texture(gl);
	return new Promise(resolve => {
		const img = new Image();
		img.onload = () => {
			texture.image = img;
			resolve(texture);
		};
		img.src = src;
	});
}

(async function () {
	const geometry = await loadModel('../../assets/airplane.json');
	const texture = await loadTexture('../../assets/airplane.jpg');

	const vertex = await loadShader('./index.vert');
	const fragment = await loadShader('./index.frag');

	const program = new Program(gl, {
		vertex,
		fragment,
		uniforms: {
			tMap: { value: texture }
		}
	});

	const mesh = new Mesh(gl, { geometry, program });
	mesh.setParent(scene);
	renderer.render({ scene, camera });

	const gui = new dat.GUI();
	const palette = {
		alpha: 0,
		beta: 0,
		theta: 0
	};
	gui.add(palette, 'alpha', -180, 180).onChange(val => {
		mesh.rotation.y = (val / 180) * Math.PI;
		renderer.render({ scene, camera });
	});
	gui.add(palette, 'beta', -180, 180).onChange(val => {
		mesh.rotation.x = (val / 180) * Math.PI;
		renderer.render({ scene, camera });
	});
	gui.add(palette, 'theta', -180, 180).onChange(val => {
		mesh.rotation.z = (val / 180) * Math.PI;
		renderer.render({ scene, camera });
	});
})();
