precision highp float;

uniform vec4 lightColor;

varying vec4 vColor;
varying float vCos;

void main() {
  gl_FragColor.rgb = vColor.rgb + vCos * lightColor.a * lightColor.rgb;
  gl_FragColor.a = vColor.a;
}