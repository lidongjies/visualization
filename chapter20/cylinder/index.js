import { multiply } from '../../common/lib/math/functions/Mat4Func.js';
import { cross, subtract, normalize } from '../../common/lib/math/functions/Vec3Func.js';
import { normalFromMat4 } from '../../common/lib/math/functions/Mat3Func.js';

function cylinder(
	radius = 1.0,
	height = 1.0,
	segments = 30,
	colorCap = [0, 0, 1, 1],
	colorSide = [1, 0, 0, 1]
) {
	const positions = [];
	const cells = [];
	const color = [];
	const cap = [[0, 0]];
	const h = 0.5 * height;
	const normal = [];

	// 顶和底的圆
	for (let i = 0; i <= segments; i++) {
		const theta = (Math.PI * 2 * i) / segments;
		const p = [radius * Math.cos(theta), radius * Math.sin(theta)];
		cap.push(p);
	}

	positions.push(...cap.map(([x, y]) => [x, y, -h]));
	normal.push(...cap.map(() => [0, 0, -1]));
	for (let i = 1; i < cap.length - 1; i++) {
		cells.push([0, i, i + 1]);
	}
	cells.push([0, cap.length - 1, 1]);

	let offset = positions.length;
	positions.push(...cap.map(([x, y]) => [x, y, h]));
	normal.push(...cap.map(() => [0, 0, 1]));
	for (let i = 1; i < cap.length - 1; i++) {
		cells.push([offset, offset + i, offset + i + 1]);
	}
	cells.push([offset, offset + cap.length - 1, offset + 1]);

	color.push(...positions.map(() => colorCap));

	const tmp1 = [];
	const tmp2 = [];
	// 侧面
	offset = positions.length;
	for (let i = 1; i < cap.length; i++) {
		const a = [...cap[i], h];
		const b = [...cap[i], -h];
		const nextIdx = i < cap.length - 1 ? i + 1 : 1;
		const c = [...cap[nextIdx], -h];
		const d = [...cap[nextIdx], h];

		positions.push(a, b, c, d);

		const norm = [];
		cross(norm, subtract(tmp1, b, a), subtract(tmp2, c, a));
		normalize(norm, norm);
		normal.push(norm, norm, norm, norm);

		color.push(colorSide, colorSide, colorSide, colorSide);
		cells.push([offset, offset + 1, offset + 2], [offset, offset + 2, offset + 3]);
		offset += 4;
	}

	return { positions, cells, color, normal };
}

const geometry = cylinder(0.5, 1.0, 30, [0, 0, 1, 1], [0.5, 0.5, 0.5, 1]);

function fromRotation(rotationX, rotationY, rotationZ) {
	let c = Math.cos(rotationX);
	let s = Math.sin(rotationX);
	const rx = [1, 0, 0, 0, 0, c, s, 0, 0, -s, c, 0, 0, 0, 0, 1];

	c = Math.cos(rotationY);
	s = Math.sin(rotationY);
	const ry = [c, 0, s, 0, 0, 1, 0, 0, -s, 0, c, 0, 0, 0, 0, 1];

	c = Math.cos(rotationZ);
	s = Math.sin(rotationZ);
	const rz = [c, s, 0, 0, -s, c, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1];

	const ret = [];
	multiply(ret, rx, ry);
	multiply(ret, ret, rz);
	return ret;
}

const canvas = document.getElementById('paper');
const renderer = new GlRenderer(canvas, { depth: true });

(async function () {
	const program = await renderer.load('./index.frag', './index.vert');
	renderer.useProgram(program);

	renderer.uniforms.projectionMatrix = [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1];
	renderer.uniforms.lightColor = [0, 1, 0, 0.8];

	renderer.setMeshData([
		{
			positions: geometry.positions,
			attributes: {
				color: geometry.color,
				normal: geometry.normal
			},
			cells: geometry.cells
		}
	]);

	let rotationX = 0;
	let rotationY = 0;
	let rotationZ = 0;

	function update() {
		rotationX += 0.003;
		rotationY += 0.005;
		rotationZ += 0.007;

		const modelMatrix = fromRotation(rotationX, rotationY, rotationZ);
		renderer.uniforms.modelMatrix = modelMatrix;
		renderer.uniforms.normalMatrix = normalFromMat4([], modelMatrix);
		requestAnimationFrame(update);
	}
	update();

	renderer.render();
})();
