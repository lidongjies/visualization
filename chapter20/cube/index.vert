attribute vec3 a_vertexPosition;
attribute vec4 color;

uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

varying vec4 vColor;

void main() {
  gl_PointSize = 1.0;
  vColor = color;
  gl_Position = projectionMatrix * modelMatrix * vec4(a_vertexPosition, 1.0);
}